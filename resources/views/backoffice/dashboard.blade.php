@extends('backoffice._template.main')
@section('content')
<!-- Page header -->
<div class="page-header page-header-default">
	<div class="page-header-content">
		<div class="page-title">
			<h4><span class="text-semibold">Dashboard</span> - Overview</h4>
		</div>
	</div>

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="{{route('backoffice.dashboard')}}"><i class="icon-home2 position-left"></i> Home</a></li>
			<li class="active">Dashboard</li>
		</ul>
	</div>
</div>
<!-- /page header -->

<!-- Content area -->
<div class="content">
	<h4 class="content-group">
		Quick Links
	</h4>

	<div class="row">
		
	</div>
	@include('backoffice._includes.footer')
</div>
<!-- /content area -->

@stop
@section('page-styles')
@stop
@section('page-scripts')
@include('backoffice._includes.page-jgrowl')
<script type="text/javascript" src="{{asset('backoffice/js/plugins/visualization/d3/d3.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/plugins/visualization/c3/c3.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/charts/c3/c3_bars_pies.js')}}"></script>

<!-- Fancybox -->
<script type="text/javascript" src="{{asset('backoffice/js/plugins/media/fancybox.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/pages/gallery.js')}}"></script>
@stop