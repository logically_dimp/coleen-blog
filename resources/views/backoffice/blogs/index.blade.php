@extends('backoffice._template.blog-template')
@section('content')
<div class="pagetitle">
	<h2>Blog</h2>
</div>
<div id="content">
	<div id="main">
		@foreach($blogs as $index => $blog)
		<!-- BLOG Thumbnail -->
		<div class="post_short">
			<!-- Image -->
			@if($blog->directory != NULL)
			<img src="{{$blog->directory}}/{{$blog->filename}}" style="max-width: 40vh; max-height: 30vh; min-height: 30vh; min-width: 40vh;">
			@endif
			@if($blog->directory == NULL)
			<img src="{{asset('backoffice/images/no-thumbnail.jpg')}}" style="max-width: 40vh; max-height: 30vh; min-height: 30vh; min-width: 40vh;">
			@endif
		</div>		<!-- .post_short ends -->
		<!-- BLOG Content -->
		<div class="post_short alt">
			<!-- Blog Title -->
			<h2><a href="{{route('backoffice.blogs.post',[$blog->id])}}">{{$blog->title}}</a></h2>
			<!-- Blog Description -->
			<p>{{$blog->excerpt}}</p>
			<span><a href="{{route('backoffice.blogs.post',[$blog->id])}}"> Read more</a></span>

			<p class="post_meta">Published on {{$blog->posted_at}}</p>
		</div>		<!-- .post_short ends -->

		<div class="clear sep"></div>
		@endforeach
	</div>		<!-- #main ends -->

	<div id="side">
	<!-- <h2>Blog categories</h2>
	<ul class="categories">
		<li><a href="#">Apple</a></li>
		<li><a href="#">Quotes</a></li>
		<li><a href="#">CSS</a></li>
		<li><a href="#">Design</a></li>
		<li><a href="#">Development</a></li>
		<li><a href="#">Music</a></li>
		<li><a href="#">Technology</a></li>
		<li><a href="#">WordPress</a></li>
	</ul> -->

	<div id="twitter">
		<ul id="twitter_update_list"><li><a class="twitter-timeline" href="https://twitter.com/coleenaaira?ref_src=twsrc%5Etfw" data-tweet-limit="1">Tweets by coleenaaira</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script></li></ul>
		<p class="twt">You should follow us on <a href="http://twitter.com/enstyled/">Twitter</a></p>
	</div>

	<ul class="ads">
		<li class="alt"><a href="http://highlysucceed.com/"><img src="{{asset('backoffice/images/AAEAAQAAAAAAAAvjAAAAJDU5N2FlYzFiLWU3NDYtNGRjNC05YTFkLWU4NTA3ZWM4MGRhNA.png')}}" style="max-width: 19vh; max-height: 19vh; min-width: 19vh; min-height: 19vh;" alt="Highly Succeed Inc." /></a></li>
		<!-- <li class="alt"><a href="http://activeden.net/?ref=enstyled"><img src="{{asset('backoffice/asset/images/activeden.gif')}}" style="max-width: 19vh; max-height: 19vh; min-width: 19vh; min-height: 19vh;" alt="ActiveDen" /></a></li> -->
		<!--
		<li><a href="http://codecanyon.net/?ref=enstyled"><img src="images/codecanyon.gif" alt="CodeCanyon" /></a></li>
		<li class="alt"><a href="http://audiojungle.net/?ref=enstyled"><img src="images/audiojungle.gif" alt="AudioJungle" /></a></li> -->
	</ul>				
</div>		<!-- #side ends -->
</div>
@stop