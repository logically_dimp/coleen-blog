@extends('backoffice._template.blog-template')
@section('content')
<div class="pagetitle">
	<h2>Blog</h2>
</div>
<div id="content">
	<div id="main" class="blogpost">

		<h3>{{$blogs->title}}</h3>
<!-- 
		<img src="{{$blogs->directory}}/{{$blogs->filename}}" style="max-width: 40vh; max-height: 30vh; min-height: 30vh; min-width: 40vh;">
 -->
		<p class="post_meta">Published on {{$blogs->posted_at}}</p>

		{!!$blogs->content!!}

	<!-- <div class="cmntshead">
		<h3>3 comments so far</h3>
		<a href="#" class="rss">Comments on this post</a>
	</div> -->

	<!-- <div id="post_comments">

		<div class="comment">
			<div class="cmnt_meta"><img src="images/avatar1.jpg" alt="" /> <strong><a href="#">Emil Nikov</a></strong> <br /><span>20.09.2009</span></div>
			<div class="cmnt_message">Pellentesque eget tortor ac nulla euismod sodales ut vel risus. Phasellus egestas, lacus ut feugiat aliquet, erat elit accumsan sapien, ac sodales ligula ipsum et nisi. Nulla nec tellus vitae nulla facilisis bibendum et eu felis. Suspendisse varius ipsum non lorem adipiscing pulvinar mattis felis placerat.</div>
		</div>


		<div class="comment">
			<div class="cmnt_meta"><img src="images/gravatar.jpg" alt="" /> <strong>Johnny Bravo</strong> <br /><span>20.09.2009</span></div>
			<div class="cmnt_message">Pellentesque eget tortor ac nulla euismod sodales ut vel risus.</div>
		</div>


		<div class="comment">
			<div class="cmnt_meta"><img src="images/gravatar.jpg" alt="" /> <strong><a href="#">John Doe</a></strong> <br /><span>20.09.2009</span></div>
			<div class="cmnt_message">Pellentesque eget tortor ac nulla euismod sodales ut vel risus. Phasellus egestas, lacus ut feugiat aliquet, erat elit accumsan sapien, ac sodales ligula ipsum et nisi.</div>
		</div>

	</div> -->	<!-- .post_comments ends -->

	<!-- <form action="#" method="post" id="comment_form">
		<h2>What do you think?</h2>
		<p><label>Full name:</label> <input type="text" class="text" /> <span>(required)</span></p>

		<p><label>E-mail:</label> <input type="text" class="text" /> <span>(required)</span></p>

		<p><label>Website:</label> <input type="text" class="text" /></p>

		<p><label>Comment:</label> <textarea cols="40" rows="10"></textarea></p>

		<p class="formend"><input type="submit" class="submit" value="Submit" /></p>
	</form> -->

	</div>

	<div id="side">

		<!-- <h2>Blog categories</h2>
		<ul class="categories">
			<li><a href="#">Apple</a></li>
			<li><a href="#">Quotes</a></li>
			<li><a href="#">CSS</a></li>
			<li><a href="#">Design</a></li>
			<li><a href="#">Development</a></li>
			<li><a href="#">Music</a></li>
			<li><a href="#">Technology</a></li>
			<li><a href="#">WordPress</a></li>
		</ul> -->

		<div id="twitter">
			<ul id="twitter_update_list"><li><a class="twitter-timeline" href="https://twitter.com/coleenaaira?ref_src=twsrc%5Etfw" data-tweet-limit="1">Tweets by coleenaaira</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script></li></ul>
			<p class="twt">You should follow us on <a href="http://twitter.com/enstyled/">Twitter</a></p>
		</div>

		<ul class="ads">
			<li class="alt"><a href="http://highlysucceed.com/"><img src="{{asset('backoffice/images/AAEAAQAAAAAAAAvjAAAAJDU5N2FlYzFiLWU3NDYtNGRjNC05YTFkLWU4NTA3ZWM4MGRhNA.png')}}" style="max-width: 19vh; max-height: 19vh; min-width: 19vh; min-height: 19vh;" alt="Highly Succeed Inc." /></a></li>
			<!-- <li class="alt"><a href="http://activeden.net/?ref=enstyled"><img src="{{asset('backoffice/asset/images/activeden.gif')}}" style="max-width: 19vh; max-height: 19vh; min-width: 19vh; min-height: 19vh;" alt="ActiveDen" /></a></li> -->
			<!-- <li class="alt"><a href="http://activeden.net/?ref=enstyled"><img src="images/activeden.gif" alt="ActiveDen" /></a></li>
			<li><a href="http://codecanyon.net/?ref=enstyled"><img src="images/codecanyon.gif" alt="CodeCanyon" /></a></li>
			<li class="alt"><a href="http://audiojungle.net/?ref=enstyled"><img src="images/audiojungle.gif" alt="AudioJungle" /></a></li> -->
		</ul>

	</div>		<!-- #side ends -->
</div>
@stop