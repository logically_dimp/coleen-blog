@extends('backoffice._template.home-template')
@section('content')
    <!--
        Start About Me Section
        ==================================== -->

    <section id="about">
        <div class="container">

            <div class="about-content">
                <div class="row">
                   <!-- Start Image & Links -->
                    <div class="col-md-5">
                        <div class="person-img">
                            <img src="{{asset('backoffice/assets/img/person.jpg')}}" alt="Fantasy" class="img-responsive">
                        </div>
                        <div class="social-links text-center">
                            <span class="fantasy-social-links"><a href="https://www.facebook.com/coleenaaira"><i  class='fa fa-facebook'></i></a></span>
                            <span class="fantasy-social-links"><a href="https://twitter.com/coleenaaira"><i  class='fa fa-twitter'></i></a></span>
                            <span class="fantasy-social-links"><a href="#" ><i  class='fa fa-behance'></i></a></span>
                            <span class="fantasy-social-links"><a href="https://www.instagram.com/airacadabra"><i  class='fa fa-linkedin'></i></a></span>
                            <span class="fantasy-social-links"><a href="#"><i  class='fa fa-pinterest'></i></a></span>
                        </div>
                    </div>
                    <!-- End Image & Links -->
                    
                    <!-- Start Information -->
                    <div class="col-md-7">
                        <div class="person-info">
                          
                           <!-- Start Top Text -->
                            <div class="person-intro">
                                <h3>Professional Profile</h3>
                                <p class="text-info">Tasked with always wearing two hats when it comes to my previous employment, I make sure that I hit the tight deadlines but deliver stellar output as well. Working in the different avenues of the corporate world, I have honed my skills in terms of multitasking, coordination, time management, as well as project management. My most recent work required me take on the role of an operations assistant while simultaneously spearheading an aspect of its digital marketing. In my years of working in the corporate field, I have likewise gained experience and developed my skills as proficient digital marketer. I am well-versed in almost every aspect of digital marketing, from content creation, social media management, as well as campaign planning and execution. </p>
                            </div>
                            <!-- End Top Text -->
                            
                            <div class="row about-skills">
                               <!-- Start About Info -->
                                <div class="col-md-7 col-sm-6">
                                    <div class="about_info">
                                       <h4>Personal Information</h4>
                                        <ul>
                                            <li>
                                                <p>Full Name</p>
                                                <span>Coleen Aira K. Barnachea</span>
                                            </li>
                                            <li>
                                                <p>D.O.B.</p>
                                                <span>1994 May 12</span>
                                            </li>
                                            <li>
                                                <p>Address</p>
                                                <span>Alberta, Canada</span>
                                            </li>
                                            <li>
                                                <p>E-Mail</p>
                                                <span>coleenbarnachea@gmail.com</span>
                                            </li>
                                            <li>
                                                <p>Phone</p>
                                                <span>+639368245807</span>
                                            </li>
                                            <li>
                                                <p>Nationality</p>
                                                <span>Filipino</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- End About Info -->
                                
                                <!-- Start Languages -->
                                <div class="col-md-5 col-sm-6">
                                    <div class="languages">
                                        <h4>Languages</h4>
                                        <!-- Start Single Languages -->
                                        <div class="single-lang">
                                            <div class="lang-title">
                                                <h5>Filipino</h5>
                                            </div>
                                            <div class="prog english">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <span class="degree">(Native)</span>
                                            </div>
                                        </div>
                                        <!-- End Single Languages -->
                                        
                                        <!-- Start Single Languages -->
                                        <div class="single-lang">
                                            <div class="lang-title">
                                                <h5>English</h5>
                                            </div>
                                            <div class="prog arabic">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star-o"></i>
                                                <i class="fa fa-star-o"></i>
                                                <span class="degree">(Fluent)</span>
                                            </div>
                                        </div>
                                        <!-- End Single Languages -->
                                        
                                    </div>
                                </div>
                                <!-- End Languages -->

                            </div>
                            <!-- End row -->
                            
                            <!-- Start Buttons -->
                            <div class="butt-resume">
                                <a href="https://www.dropbox.com/s/inis2eop7gdl5fl/UPDATED%20RESUME.pdf">Download Resume</a>
                            </div>
                            <!-- End Buttons -->

                        </div><!-- End person-info -->
                    </div><!-- End Information -->
                </div><!-- End row -->
            </div><!-- End about-content -->

        </div><!-- End Container -->
    </section><!-- End Section -->

    <!--
        End About Me Section
        ==================================== -->

    <!--
        Start Statistic Section
        ==================================== -->

    <section id="statistic" class="text-center">
        <div class="container">
            <div class="row">

               <!-- Start Experience Statis -->
                <div class="col-sm-4 wow fadeInUp" data-wow-duration=".8s" data-wow-delay="200ms">
                    <div class="exp-statis">
                        <h4>Experience</h4>
                        <div class="statistic-block">
                            <h2 class="statistic-number">2</h2>
                            <p>Years</p>
                        </div>
                    </div>
                </div>
                <!-- End Experience Statis -->

               <!-- Start Rate Statis -->
                <div class="col-sm-4 wow fadeInUp" data-wow-duration=".8s" data-wow-delay="400ms">
                    <div class="rate-statis">
                        <h4>Rate</h4>
                        <div class="statistic-block">
                            <span class="coin">$</span>
                            <h2 class="statistic-number">35</h2>
                            <p>Per Hour</p>
                        </div>
                    </div>
                </div>
                <!-- End Rate Statis -->

               <!-- Start Work Statis -->
                <div class="col-sm-4 wow fadeInUp" data-wow-duration=".8s" data-wow-delay="600ms">
                    <div class="work-statis">
                        <h4>Work</h4>
                        <div class="statistic-block">
                            <h2 class="statistic-number">26</h2>
                            <p>Projects Done</p>
                        </div>
                    </div>
                </div>
                <!-- End Work Statis -->

            </div><!-- End row -->
        </div><!-- End Container -->
    </section><!-- End Section -->

    <!--
        End Statistic Section
        ==================================== -->
        
    <!--
        Start Skills Section
        ==================================== -->
        
        <section id="skills" class="add-padding border-bottom-color2">
        
            <div class="container">
                
                <div class="row">
                
                    <div class="col-sm-6 col-md-5">
                        <div class="skill-title">
                            <h2 class="h1">I Got The Skills</h2>
                            <p class="small-text-skill">In my years of working in the Marketing and Information Technology scenes in the Philippines, as well as several stints working part-time as an online freelancer, I have learned the necessary skill set needed for any industry job.</p>
                        </div>
                    </div>
                
                    <div class="col-sm-6 col-md-5 col-md-offset-1">
                    
                        <div class="skills-bars text-center">
                        
                            <div class="skills-item skill1">
                                <span class="percent" data-percent="80">Account/Project Management</span>
                                <p>80%</p>
                            </div>
                            <div class="skills-item skill2">
                                <span class="percent" data-percent="90">Content Curation/Writing</span>
                                <p>90%</p>
                            </div>
                            <div class="skills-item skill3">
                                <span class="percent" data-percent="70">Social Media Marketing</span>
                                <p>70%</p>
                            </div>
                            <div class="skills-item skill4">
                                <span class="percent" data-percent="85">Concept Development</span>
                                <p>85%</p>
                            </div>
                            
                        </div>
                    
                    </div>
                    
                </div>
                
            </div>
        
        </section>
        
    <!--
        End Skills Section
        ==================================== -->

    <!--
        Start Portfolio Section
        ==================================== -->

    <section id="portfolio">
        <div class="container">
           
            

        </div><!-- End Container -->
    </section><!-- End Section -->

    <!--
        End Portfolio Section
        ==================================== -->

    <!--
        Start Resume Section
        ==================================== -->

    <section id="resume">
        <div class="container">
           
            <!-- Start Section Title -->
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title text-center">
                        <h4>What I Did</h4>
                        <h2 class="h1">Resume</h2>
                    </div>
                </div>
            </div>
            <!-- End Section Title -->

            <div class="row content-centered">
                <div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2">
                   <!-- Start Timeline Centered -->
                    <ul class="timeline timeline-centered">
                       <!-- Start Experience Title -->
                        <li class="timeline-item period">
                            <div class="timeline-info"></div>
                            <div class="timeline-marker exp-title"></div>
                            <div class="timeline-content">
                                <h2 class="timeline-title">Experience</h2>
                            </div>
                        </li>
                        <!-- End Experience Title -->
                        
                        <!-- Start Single Item -->
                        <li class="timeline-item">
                            <div class="timeline-info wow fadeInRight" data-wow-duration=".8s" data-wow-delay="200ms">
                                <span>2017 - 2018</span>
                            </div>
                            <div class="timeline-marker"></div>
                            <div class="timeline-content wow fadeInLeft" data-wow-duration=".8s" data-wow-delay="200ms">
                                <h3 class="timeline-title">Operations Assistant</h3>
                                <p>Ensures smooth and timely office-wide operations, managing documentation while improving processes streamlining procedures of the department.</p>
                            </div>
                        </li>
                        <!-- End Single Item -->
                        
                        <!-- Start Single Item -->
                        <li class="timeline-item">
                            <div class="timeline-info wow fadeInLeft" data-wow-duration=".8s" data-wow-delay="200ms">
                                <span>2016 - 2017</span>
                            </div>
                            <div class="timeline-marker"></div>
                            <div class="timeline-content wow fadeInRight" data-wow-duration=".8s" data-wow-delay="200ms">
                                <h3 class="timeline-title">SEO Content Writer</h3>
                                <p>Write up to 30 pieces of content weekly using SEO keywords drawn from Google Analytics.</p>
                            </div>
                        </li>
                        <!-- End Single Item -->
                        
                        <!-- Start Single Item -->
                        <li class="timeline-item">
                            <div class="timeline-info wow fadeInRight" data-wow-duration=".8s" data-wow-delay="200ms">
                                <span>2015 - 2016</span>
                            </div>
                            <div class="timeline-marker"></div>
                            <div class="timeline-content wow fadeInLeft" data-wow-duration=".8s" data-wow-delay="200ms">
                                <h3 class="timeline-title">Account Management Officer</h3>
                                <p>Handles and process customer’s transactions in compliance with business rules, policies and regulations.</p>
                            </div>
                        </li>
                        <!-- End Single Item -->
                        
                        <!-- Start Education Title -->
                        <li class="timeline-item period">
                            <div class="timeline-info"></div>
                            <div class="timeline-marker"></div>
                            <div class="timeline-content">
                                <h2 class="timeline-title">Education</h2>
                            </div>
                        </li>
                        <!-- End Education Title -->
                        
                        <!-- Start Single Item -->
                        <li class="timeline-item">
                            <div class="timeline-info wow fadeInLeft" data-wow-duration=".8s" data-wow-delay="200ms">
                                <span>2012 - 2015</span>
                            </div>
                            <div class="timeline-marker"></div>
                            <div class="timeline-content wow fadeInRight" data-wow-duration=".8s" data-wow-delay="200ms">
                                <h3 class="timeline-title">Bachelor of Arts in Communication</h3>
                                <p>
                                    TRINITY UNIVERSITY OF ASIA <br>
                                    375 E. Rodriguez Sr. Avenue, Quezon City
                                </p>
                            </div>
                        </li>
                        <!-- End Single Item -->
                        
                        <!-- Start Single Item -->
                        <li class="timeline-item">
                            <div class="timeline-info wow fadeInRight" data-wow-duration=".8s" data-wow-delay="200ms">
                                <span>2010 - 2011</span>
                            </div>
                            <div class="timeline-marker"></div>
                            <div class="timeline-content wow fadeInLeft" data-wow-duration=".8s" data-wow-delay="200ms">
                                <h3 class="timeline-title">Bachelor of Science in Nutrition and Dietetics</h3>
                                <p>
                                    CENTRO ESCOLAR UNIVERSITY <br>
                                    9 Mendiola, San Miguel, Manila
                                </p>
                            </div>
                        </li>
                        <!-- End Single Item -->
                        
                    </ul>
                    <!-- End Timeline Centered -->
                </div><!-- col-xs-10 -->
            </div><!-- content-centered -->

        </div><!-- End Container -->
    </section><!-- End Section -->

    <!--
        End Resume Section
        ==================================== -->
        
    <!--
        Start Hire Me Section
        ==================================== -->
        
        <section id="hire">
           <div class="overlay">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 text-center hire">
                            <div class="hire-content">
                                <h3>Do you have an interesting project?</h3>
                                <p>I am available for freelance projects.</p>
                            </div>
                            <div class="hire-butt">
                                <a  class="hire-me" href="#contact">Get Quotes</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
    <!--
        End Hire Me Section
        ==================================== -->

    <!--
        Start Contact Section
        ==================================== -->

    <section id="contact">
        <div class="container">
           
            <!-- Start Section Title -->
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title text-center">
                        <h4>Get In Touch</h4>
                        <h2 class="h1">Contact</h2>
                    </div>
                </div>
            </div>
            <!-- End Section Title -->

            <div class="row">

                <div class="contact-content">

                    <!-- Start Google Map -->
                    <div class="col-md-6">
                        <div class="google-map-area">
                            <div id="contacts" class="map-area">
                              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d303375.89923862746!2d-113.77412889113396!3d53.555550085586766!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x53a0224580deff23%3A0x411fa00c4af6155d!2sEdmonton%2C+AB%2C+Canada!5e0!3m2!1sen!2sph!4v1518510156123" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                    <!-- End Google Map -->

                   <!-- Start Contact Form -->
                    <div class="col-md-6">
                        <form class='form' id='contact-form' method='post'><input type='hidden' name='form-name' value='contact-form' />
                            <div class="contact-form">
                                <input type="text" name="name" id="name" placeholder="Name" required>
                                <input type="email" name="email" id="email" placeholder="E-Mail" required>
                                <input type="text" name="subject" id="subject" placeholder="Subject" required>
                                <textarea name="message" id="message" placeholder="Your Message" required></textarea>
                            </div>
                            <div id="mail-success" class="success">
                                Thank you. Your Mail On His Way :)
                            </div>
                            <div id="mail-fail" class="error">
                                Sorry, an error occurred. Try later :(
                            </div>
                            <div class="butt-msg">
                                <input type="submit" id="contact-submit" value="Send Message">
                            </div>
                        </form>
                    </div>
                    <!-- End Contact Form -->

                </div><!-- contact-content -->
                
            </div><!-- End row -->
        </div><!-- End Container -->
    </section><!-- End Section -->

    <!--
        End Contact Section
        ==================================== -->
@stop
    
