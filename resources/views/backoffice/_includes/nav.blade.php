	<li class="{{Helper::is_active($routes['dashboard'])}}">
		<a href="{{route('backoffice.dashboard')}}"><i class="icon-home4"></i> <span>Dashboard</span></a>
	</li>
	<li class="navigation-header"><span>Mobile Application</span> <i class="icon-menu" title="Mobile Application"></i></li>

	<li>
		<a href="#"><i class="icon-mobile"></i> <span>Content Management</span></a>
		<ul>
			<li class="">
				<a href="#"><i class="icon-bubble-notification"></i> <span>Blogs</span></a>
				<ul>
					<li class=""><a href="">Record Data</a></li>
					<li class=""><a href="">Create New</a></li>
				</ul>
			</li>
		</ul>
	</li>

	@if($auth->type == "super_user")
	<li class="navigation-header"><span>User Management</span> <i class="icon-menu" title="User Management"></i></li>

	<li class="{{Helper::is_active($routes['admins'],'active')}}">
		<a href="#"><i class="icon-user"></i> <span>Administrator Account</span></a>
		<ul>
			<li class="{{Helper::is_active([$routes['admins'][0]])}}"><a href="{{route('backoffice.admins.index')}}">Record Data</a></li>
		</ul>
	</li>
	@endif