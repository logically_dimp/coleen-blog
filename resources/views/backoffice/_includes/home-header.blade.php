<!--
    Start Header
    ==================================== -->

    <header id="home">
        <div class="overlay-strip">
            <div class="overlay">

                <!-- Start Navigation Bar -->

                <div class="navbar custom-navbar navbar-fixed-top" role="navigation">
                    <div class="container">

                        <!-- Mobile Menu -->
                        <div class="navbar-header">
                            <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="icon icon-bar"></span>
                                <span class="icon icon-bar"></span>
                                <span class="icon icon-bar"></span>
                            </button>
                            <!-- Logo Here -->
                            <a href='index.html' class='navbar-brand'>Coleen</a>
                        </div>

                        <!-- Navigation Links -->
                        <div class="collapse navbar-collapse">
                            <ul class="nav navbar-nav navbar-right">
                                <li class="active"><a class="smoothScroll" href="#home">Home</a></li>
                                <li><a class="smoothScroll" href="#about">About</a></li>
                                <li><a class="smoothScroll" href="{{route('backoffice.blogs.index')}}">My Blogs</a></li>
                                <li><a class="smoothScroll" href="#resume">Resume</a></li>
                                <li><a class="smoothScroll" href="#contact">Contact</a></li>
                            </ul>
                        </div>

                    </div><!-- End Container Nav -->
                </div>

                <!-- End Navigation Bar -->

                <div class="container">

                   <!-- Start Text Header -->
                   <div class="header-inner">
                    <div class="header-content text-center">
                        <h1 class="home-title">HI! I'm <span>Coleen Aira</span></h1>
                        <h4><span class="typewrite" data-period="2000" data-type='[ "Freelance Content Developer.", "Enterprising Digital Marketer.", "A Go-Getter."]'></span></h4>
                    </div>
                </div>
                <!-- End Text Header -->

                <!-- Start Button To Down -->
                <div class="but-down text-center">
                    <a href="#about"><i class="fa fa-angle-down"></i></a>
                </div>
                <!-- Start Button To Down -->

            </div><!-- End Container -->
        </div><!-- End Overlay -->
    </div><!-- End Overlay Strip -->
</header><!-- End Header -->
    <!--
        End Header
        ==================================== -->