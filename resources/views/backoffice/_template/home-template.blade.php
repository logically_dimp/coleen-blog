<!DOCTYPE html>
<html lang="en">
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<!--<![endif]-->


<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <!-- Basic Page Needs -->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="description" content="Coleen - Personal Portfolio">
    <meta name="author" content="Coleen Aira Barnachea">

    <!-- Page Title -->
    <title>Coleen - Personal Portfolio</title>

    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/icon.png" />

    <!--
            Google Font
            ================================================== -->

    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700|Quicksand" rel="stylesheet">

    <!--
            CSS Files
            ================================================== -->

    <!-- Bootstrap v3.7 -->
    <link rel='stylesheet' href="{{asset('backoffice/assets/css/bootstrap.min.css')}}"/>
    <!-- Animate -->
    <link rel='stylesheet' href="{{asset('backoffice/assets/css/animate.css')}}"/>
    <!-- Magnific Popup -->
    <link rel='stylesheet' href="{{asset('backoffice/assets/css/magnific-popup.css')}}"/>
    <!-- Carousel -->
    <link rel='stylesheet' href="{{asset('backoffice/assets/css/d79eb085dd37237fb7f75315387b7bdb265c60e5.css')}}"/>
    
    <!-- Main Style -->
    <link rel='stylesheet' href="{{asset('backoffice/assets/css/style.css')}}"/>
    <!-- Responsive File -->
    <link rel='stylesheet' href="{{asset('backoffice/assets/css/responsive.css')}}"/>

    <!-- Modernizer Script for old Browsers -->
    <script src="{{asset('backoffice/assets/js/modernizr-2.6.2.min.js')}}"></script>
</head>

<body>

    @include('backoffice._includes.home-header')

    @yield('content')
    <!--
        Start Footer
        ==================================== -->

    <footer id="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="social-links text-center">
                        <span class="fantasy-social-links"><a href="https://www.facebook.com/coleenaaira"><i  class='fa fa-facebook'></i></a></span>
                        <span class="fantasy-social-links"><a href="https://twitter.com/coleenaaira"><i  class='fa fa-twitter'></i></a></span>
                        <span class="fantasy-social-links"><a href="#" ><i  class='fa fa-behance'></i></a></span>
                        <span class="fantasy-social-links"><a href="https://www.instagram.com/airacadabra"><i  class='fa fa-linkedin'></i></a></span>
                        <span class="fantasy-social-links"><a href="#"><i  class='fa fa-pinterest'></i></a></span>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!--
        End Footer
        ==================================== -->

    <!--
        Start Back To Top
        ==================================== -->

    <div id="scroll-top">
        <i class="fa fa-angle-up fa-2x"></i>
    </div>

    <!--
        End Back To Top
        ==================================== -->

    <!--
        Start Preloader
        ==================================== -->

    <div id="loading-mask">
        <div class="spinner">
            <div class="rect1"></div>
            <div class="rect2"></div>
            <div class="rect3"></div>
            <div class="rect4"></div>
            <div class="rect5"></div>
        </div>
    </div>

    <!--
        End Preloader
        ==================================== -->

    <!--
            JS Files
            ==================================== -->

    <!-- Main jQuery -->
    <script src="{{asset('backoffice/assets/js/jquery-3.2.1.min.js')}}"></script>
    <!-- Bootstrap v3.7 -->
    <script src="{{asset('backoffice/assets/js/bootstrap.min.js')}}"></script>
    <!-- Wow Plugin -->
    <script src="{{asset('backoffice/assets/js/wow.min.js')}}"></script>
    <!-- Carousel -->
    <script src="{{asset('backoffice/assets/js/owl.carousel.min.js')}}"></script>
    <!-- CountTo -->
    <script src="{{asset('backoffice/assets/js/jquery.countto.js')}}"></script>
    <!-- Appear JS -->
    <script src="{{asset('backoffice/assets/js/jquery.appear.js')}}"></script>
    <!-- Portfolio Full Screen Picture -->
    <script src="{{asset('backoffice/assets/js/jquery.magnific-popup.min.js')}}"></script>
    <!-- Portfolio Filtering -->
    <script src="{{asset('backoffice/assets/js/ac0b3f195d5ad9bfdd95b5687cb1848649d19823.js')}}"></script>
    <!-- Google Map js -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAj9b_nyz33KEaocu6ZOXRgqwwUZkDVEAw"></script>
    <!-- Custom JS -->
    <script src="{{asset('backoffice/assets/js/custom.js')}}"></script>

</body>


</html>