<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>My Blogs</title>
	<meta name="description" content="..." />
	<meta name="keywords" content="..." />
	<style type="text/css" media="all">
		@import url("{{asset('backoffice/asset/css/style.css')}}");
		@import url("{{asset('backoffice/asset/css/nivo-slider.css')}}");
		@import url("{{asset('backoffice/asset/css/custom-nivo-slider.css')}}");
		@import url("{{asset('backoffice/asset/css/jquery.fancybox.css')}}");
	</style>
	<!--[if lt IE 8]><style type="text/css" media="all">@import url("css/ie.css");</style><![endif]-->
</head>

<body>

	<div id="bgc">				
		<div class="wrapper">		<!-- wrapper begins -->
			<div id="header">
				<ul>
					<li><a href="{{route('backoffice.home')}}">Coleen</a></li>
					<li><a href="{{route('backoffice.blogs.index')}}" class="active">Blog</a></li>
				</ul>
			</div>		<!-- #header ends -->
			<div id="holder">
				
					@yield('content')
			</div>		<!-- #holder ends -->
			
			<!-- 
			<div id="logos">
				<ul>
					<li><a href="http://php.net/"><img src="images/php.png" alt="PHP" /></a></li>
					<li><a href="http://mysql.com/"><img src="images/mysql.png" alt="MySQL" /></a></li>
					<li><a href="http://jquery.com/"><img src="images/jquery.png" alt="jQuery" /></a></li>
					<li><a href="http://wordpress.org/"><img src="images/wp.png" alt="WordPress" /></a></li>
					<li><a href="http://expressionengine.com/"><img src="images/ee.png" alt="Expression Engine" /></a></li>
				</ul>
			</div>	 -->	<!-- #logos ends -->
			
			<div id="footer">
				<p class="left"><span>Coleen</span></p>
				<p class="right">Copyright &copy; 2018. Some rights reserved.</p>
			</div>		<!-- #footer ends -->
			
		</div>		<!-- wrapper ends -->
		
	</div>

	<script type="text/javascript" src="{{asset('backoffice/asset/js/jquery.js')}}"></script>
	<script type="text/javascript" src="{{asset('backoffice/asset/js/jquery.nivo.slider.pack.js')}}"></script>
	<script type="text/javascript" src="{{asset('backoffice/asset/js/jquery.fancybox.pack.js')}}"></script>
	<!--[if lt IE 8]><script type="text/javascript" src="js/DD_belatedPNG.js"></script><![endif]-->
	<script type="text/javascript" src="{{asset('backoffice/asset/js/custom.js')}}"></script>
	
	<!-- Twitter badge-->
	<script type="text/javascript" src="../../twitter.com/javascripts/blogger.html"></script>
	<script type="text/javascript" src="http://api.twitter.com/1/statuses/user_timeline.json?callback=twitterCallback2&amp;screen_name=enstyled&amp;count=1&amp;include_rts=true"></script>
	
</body>
</html>