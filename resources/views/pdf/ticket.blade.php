<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>TICKET</title>
	<style type="text/css">
		@page { margin: 0px; }
		html { margin: 5px 10px}
		body,p{ padding: 0px; margin: 0px; }
		body, #wrapper, #content {
		font-family:sans-serif;
		}
		table{
		font-family:sans-serif;
		}     
		table tr{
		font-family:sans-serif;
		}    
		table td{
		font-family:sans-serif;
		}        
		a:link, a:visited {
		font-family:sans-serif;
		}
		p {
		font-family:sans-serif;
		}
	</style>
</head>
<body>
	<p style="text-align: center; font-weight: 600; font-size: 12px;">MAYOR'S ACTION CENTER</p>
	<p style="text-align: center; font-size: 5px; padding-bottom: 10px; border-bottom: 1px solid #333;">{{Helper::date_format($queue->created_at)}}</p>
	<p style="font-size: 55px; font-weight: bolder; text-align: center;">{{$queue->queue_no}}</p>
	<p style="text-align: center; font-weight: 600;"><span style="background: #000; color: #fff; padding: 5px; margin-right: 10px;">{{$queue->is_priority == "yes" ? "P" : ""}}</span>{{Str::upper(Helper::get_service_display($queue->service_type ,"title"))}}</p>
	<p style="font-size: 10px; text-align: center">-- {{$queue->name}} --</p>
	<p style="font-size: 8px; margin-top: 10px; text-align: center; border-bottom: 1px solid #999;">Your number will be called shortly, thank you for waiting.</p>
	<p style="font-size: 7px; margin-top: 5px; text-align: center;">Powered by : Information Technology Services Division</p>

	{{--<p style='background: #333; color: #fff;text-align: center;font-size: 24px;  font-weight: bold;'>{{Helper::get_service_display($queue->service_type ,"title")}}</p>
	<hr style="margin-bottom: 20px;">
	<p style="font-size: 14px; position: absolute;bottom: 15;">-- {{$queue->name}} --</p>
	<p style="font-size: 14px; position: absolute;bottom: 0px;">{{Helper::date_format($queue->created_at)}}</p>--}}


	
</body>
</html>