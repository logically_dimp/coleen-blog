<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
        ],
        'web-no-crsf' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
        ],
        'api' => [
            // 'throttle:60,1',
            \App\Laravel\Middleware\Api\FormatFilter::class,
            \App\Laravel\Middleware\Api\ApiTokenizer::class,
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \App\Http\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'can' => \Illuminate\Foundation\Http\Middleware\Authorize::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,

        'backoffice.auth' => \App\Laravel\Middleware\Backoffice\Authenticate::class,
        'backoffice.guest' => \App\Laravel\Middleware\Backoffice\RedirectIfAuthenticated::class,
        'backoffice.lock' => \App\Laravel\Middleware\Backoffice\Lock::class,
        'backoffice.zone-verifier' => \App\Laravel\Middleware\Backoffice\ZoneVerifier::class,
        'backoffice.super-user-only' => \App\Laravel\Middleware\Backoffice\SuperUserOnly::class,
        'backoffice.authorize' => \App\Laravel\Middleware\Backoffice\Authorize::class,
       
        'api.tokenizer' => \App\Laravel\Middleware\Api\ApiTokenizer::class,
        'api.auth' => \App\Laravel\Middleware\Api\Authenticate::class,
        'api.valid-notice' => \App\Laravel\Middleware\Api\ValidNotice::class,
        'api.valid-social' => \App\Laravel\Middleware\Api\ValidSocial::class,
        'api.valid-directory' => \App\Laravel\Middleware\Api\ValidDirectory::class,
        'api.valid-gallery' => \App\Laravel\Middleware\Api\ValidGallery::class,
        'api.valid-emergency-directory' => \App\Laravel\Middleware\Api\ValidEmergencyDirectory::class,
        'api.valid-emergency-establishment' => \App\Laravel\Middleware\Api\ValidNonEmergencyEstablishment::class,
        'api.valid-non-emergency-directory' => \App\Laravel\Middleware\Api\ValidNonEmergencyDirectory::class,
        'api.valid-non-emergency-establishment' => \App\Laravel\Middleware\Api\ValidEmergencyEstablishment::class,
        'api.valid-article' => \App\Laravel\Middleware\Api\ValidArticle::class,
        'api.valid-report' => \App\Laravel\Middleware\Api\ValidReport::class,
        'api.valid-establishment' => \App\Laravel\Middleware\Api\ValidEstablishment::class,
        'api.valid-request' => \App\Laravel\Middleware\Api\ValidRequest::class,
        'api.valid-service' => \App\Laravel\Middleware\Api\ValidService::class,
        'api.valid-subservice' => \App\Laravel\Middleware\Api\ValidSubservice::class,
        'api.valid-page' => \App\Laravel\Middleware\Api\ValidPage::class,
        'api.valid-moment' => \App\Laravel\Middleware\Api\ValidMoment::class,
        'api.valid-classified' => \App\Laravel\Middleware\Api\ValidWidget::class,
        'api.valid-queue' => \App\Laravel\Middleware\Api\ValidQueue::class,
        'api.valid-queue-for-display' => \App\Laravel\Middleware\Api\ValidQueueForDisplay::class,
        'api.valid-teller' => \App\Laravel\Middleware\Api\ValidTeller::class,
    ];
}
