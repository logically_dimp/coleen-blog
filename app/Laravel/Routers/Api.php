<?php

$router->group([

	/**
	*
	* Api routes main config
	*/
	'namespace' => "Api",
	'as' => "api.",
	// 'domain' => env('BACKOFFICE_URL',"backoffice.pepero.localhost.com"),
	'prefix' => "api",
	'middleware'=> "api"

	], function(){

	/**
	*
	* Summernote upload route
	*/
	$this->post('summernote-upload.{data_format?}',['as' => "summernote",'uses' => "SummernoteController@upload"]);

	/**
	*
	* Authentication route
	*/
	$this->group(['prefix' => "auth"],function(){
		// $this->post('register.{data_format?}',['uses' => "AuthController@store"]);
		$this->post('login.{data_format?}',['uses'	=> "AuthController@authenticate"]);
		$this->post('fb-login.{data_format?}',['uses'	=> "AuthController@fb_login"]);
		$this->any('logout.{data_format}',['uses' => 'AuthController@destroy']);
		$this->any('remove-email.{data_format}',['uses' => 'AuthController@remove_email']);
		$this->any('forgot-password.{data_format}',['uses' => 'AuthController@forgot_password']);
		$this->any('reset-password.{data_format}',['uses' => 'AuthController@reset_password']);
		//$this->any('resend-activation.{data_format}',['uses' => 'AuthController@resend_activation']);
	});

	$this->group(['middleware' => "api.auth"],function(){

		/**
		*
		* User route
		*/
		$this->group(['prefix' => "user"],function(){
			$this->any('profile.{data_format}',['uses' => 'UserController@profile']);
			$this->group(['prefix' => "update"],function(){
				$this->post('profile.{data_format}',['uses' => 'UserController@update_profile']);
				$this->post('password.{data_format}',['uses' => 'UserController@update_password']);
				$this->post('avatar.{data_format}',['uses' => 'UserController@update_avatar']);
				$this->any('field.{data_format}',[ 'uses' => "UserController@update_field"]);
				$this->any('reg-id.{data_format}',[ 'uses' => "UserController@update_reg_id"]);
			});
		});

		/**
		*
		* Notice route
		*/
		$this->group(['prefix' => "notice"],function(){
			$this->any('all.{data_format}',['uses' => 'NoticeController@index']);
			$this->any('recent.{data_format}',['uses' => 'NoticeController@recent']);
			$this->any('featured.{data_format}',['uses' => 'NoticeController@featured']);
			$this->group(['middleware' => "api.valid-notice"],function(){
				$this->any('show.{data_format}',['uses' => 'NoticeController@show']);
				$this->any('comment.{data_format?}',['uses' => 'NoticeCommentController@store']);
				$this->any('delete-comment.{data_format?}',['uses' => 'NoticeCommentController@destroy']);
				$this->any('like.{data_format?}',['uses' => 'NoticeLikeController@store']);
				$this->any('unlike.{data_format?}',['uses' => 'NoticeLikeController@destroy']);
			});
		});

		/**
		*
		* Social route
		*/
		$this->group(['prefix' => "social"],function(){
			$this->any('all.{data_format}',['uses' => 'SocialController@index']);
			$this->any('recent.{data_format}',['uses' => 'SocialController@recent']);
			$this->any('featured.{data_format}',['uses' => 'SocialController@featured']);
			$this->group(['middleware' => "api.valid-social"],function(){
				$this->any('show.{data_format}',['uses' => 'SocialController@show']);
				$this->any('comment.{data_format?}',['uses' => 'SocialCommentController@store']);
				$this->any('comment-list.{data_format?}',['uses' => 'SocialCommentController@list']);
				$this->any('delete-comment.{data_format?}',['uses' => 'SocialCommentController@destroy']);
				$this->any('like.{data_format?}',['uses' => 'SocialLikeController@store']);
				$this->any('unlike.{data_format?}',['uses' => 'SocialLikeController@destroy']);
			});
		});

		/**
		*
		* Citizen Report route
		*/
		$this->group(['prefix' => "citizen-report"],function(){
			$this->any('all.{data_format?}',['uses' => "CitizenReportController@index"]);
			$this->any('types.{data_format?}',['uses' => "CitizenReportController@types"]);
			$this->group(['middleware' => "api.valid-report"],function(){
				$this->any('show.{data_format}',['uses' => 'CitizenReportController@show']);
				$this->any('delete.{data_format?}',['uses' => "CitizenReportController@destroy"]);
			});
			$this->any('my-reports.{data_format?}',['uses' => "CitizenReportController@my_reports"]);
			$this->any('create.{data_format?}',['uses' => "CitizenReportController@store"]);
			$this->any('pending.{data_format?}',['uses' => "CitizenReportController@pending"]);
			$this->any('on-going.{data_format?}',['uses' => "CitizenReportController@on_going"]);
			$this->any('resolved.{data_format?}',['uses' => "CitizenReportController@resolved"]);
		});

		/**
		*
		* Directory route
		*/
		$this->group(['prefix' => "directory"],function(){
			$this->any('all.{data_format}',['uses' => 'DirectoryController@index']);
			$this->any('recent.{data_format}',['uses' => 'DirectoryController@recent']);
			$this->any('featured.{data_format}',['uses' => 'DirectoryController@featured']);
			$this->group(['middleware' => "api.valid-directory"],function(){
				$this->any('show.{data_format}',['uses' => 'DirectoryController@show']);
			});
		});

		/**
		*
		* Classified route
		*/
		$this->group(['prefix' => "classified"],function(){
			$this->any('all.{data_format}',['uses' => 'ClassifiedController@index']);
			$this->any('recent.{data_format}',['uses' => 'ClassifiedController@recent']);
			$this->any('featured.{data_format}',['uses' => 'ClassifiedController@featured']);
			$this->group(['middleware' => "api.valid-notice"],function(){
				$this->any('show.{data_format}',['uses' => 'ClassifiedController@show']);
			});
		});

		/**
		*
		* Gallery route
		*/
		$this->group(['prefix' => "gallery"],function(){
			$this->any('all.{data_format}',['uses' => 'GalleryController@index']);
			$this->any('recent.{data_format}',['uses' => 'GalleryController@recent']);
			$this->any('featured.{data_format}',['uses' => 'GalleryController@featured']);
			$this->group(['middleware' => "api.valid-gallery"],function(){
				$this->any('show.{data_format}',['uses' => 'GalleryController@show']); 
			});
		});

		/**
		*
		* Emergency Directory route
		*/
		$this->group(['prefix' => "emergency-directory"],function(){
			$this->any('all.{data_format?}',['uses' => "EmergencyDirectoryController@index"]);
			$this->group(['middleware' => "api.valid-emergency-directory"],function(){
				$this->any('show.{data_format}',['uses' => 'EmergencyDirectoryController@show']);
			});
		});

		/**
		*
		* Emergency Establishment route
		*/
		$this->group(['prefix' => "emergency-establishment"],function(){
			$this->any('all.{data_format?}',['uses' => "EmergencyEstablishmentController@index"]);
			$this->group(['middleware' => "api.valid-emergency-establishment"],function(){
				$this->any('show.{data_format}',['uses' => 'EmergencyEstablishmentController@show']);
				$this->any('list.{data_format?}',['uses' => "EmergencyEstablishmentController@establishment_list"]);
			});
		});

		/**
		*
		* Non Emergency Directory route
		*/
		$this->group(['prefix' => "non-emergency-directory"],function(){
			$this->any('all.{data_format?}',['uses' => "NonEmergencyDirectoryController@index"]);
			$this->group(['middleware' => "api.valid-non-emergency-directory"],function(){
				$this->any('show.{data_format}',['uses' => 'NonEmergencyDirectoryController@show']);
			});
		});

		/**
		*
		* Non Emergency Establishment route
		*/
		$this->group(['prefix' => "non-emergency-establishment"],function(){
			$this->any('all.{data_format?}',['uses' => "NonEmergencyEstablishmentController@index"]);
			$this->group(['middleware' => "api.valid-non-emergency-establishment"],function(){
				$this->any('show.{data_format}',['uses' => 'NonEmergencyEstablishmentController@show']);
				$this->any('list.{data_format?}',['uses' => "NonEmergencyEstablishmentController@establishment_list"]);
			});
		});
	});


});
