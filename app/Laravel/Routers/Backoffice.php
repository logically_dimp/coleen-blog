<?php

$router->group([

	/**
	*
	* Backend routes main config
	*/
	'namespace' => "Backoffice", 
	'as' => "backoffice.", 
	// 'domain' => env('BACKOFFICE_URL',"backoffice.pepero.localhost.com"), 
	'middleware'=> "web"

	], function(){

	$this->get('google673373b13476671e.html',['as' => "google673373b13476671e.html", 'uses' => "DashboardController@google"]);

	$this->get('activate/{activation_token?}', ['as' => "activate", 'uses' => "AuthController@activate"]);


	/**
	*
	* Routes for the homepage
	*/
	$this->get('/',['as' => "home",'uses' => "HomeController@index"]);

	$this->group(['prefix' => "blogs", 'as' => "blogs."], function(){
		$this->get('/',['as' => "index",'uses' => "BlogController@index"]);
		$this->get('all',['as' => "all",'uses' => "BlogController@all"]);
		$this->any('post/{id?}',['as' => "post",'uses' => "BlogController@posts"]);
	});
		
	/**
	*
	* Routes for guests
	*/
	$this->group(['middleware' => "backoffice.guest"], function(){
		$this->get('login',['as' => "login",'uses' => "AuthController@login"]);
		$this->post('login',['uses' => "AuthController@authenticate"]);
	});

	/**
	*
	* Routes for authenticated users
	*/
	$this->group(['middleware' => "backoffice.auth"], function(){
		
		$this->get('lock',['as' => "lock",'uses' => "AuthController@lock"]);
		$this->post('lock',['uses' => "AuthController@unlock"]);
		$this->get('logout',['as' => "logout",'uses' => "AuthController@destroy"]);

		$this->group(['middleware' => ["backoffice.lock","backoffice.authorize"] ], function(){
			$this->get('dashboard',['as' => "dashboard",'uses' => "DashboardController@index"]);

			$this->group(['prefix' => "admins", 'as' => "admins."], function(){
				$this->get('/',['as' => "index",'uses' => "AdminController@index"]);
				$this->get('create',['as' => "create",'uses' => "AdminController@create"]);
				$this->post('create',['uses' => "AdminController@store"]);
				$this->get('edit/{id?}',['as' => "edit",'uses' => "AdminController@edit"]);
				$this->post('edit/{id?}',['uses' => "AdminController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "AdminController@destroy"]);
			});

			$this->group(['prefix' => "posts", 'as' => "posts."], function (){
				$this->get('/',['as' => "index", 'uses' => "BlogController@main"]);
				$this->get('create',['as' => "create",'uses' => "BlogController@create"]);
				$this->post('create',['uses' => "BlogController@store"]);
				$this->get('edit/{id?}',['as' => "edit",'uses' => "BlogController@edit"]);
				$this->post('edit/{id?}',['uses' => "BlogController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "BlogController@destroy"]);
			});

		});
	});
});
