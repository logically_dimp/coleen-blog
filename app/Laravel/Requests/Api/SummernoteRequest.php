<?php 

namespace App\Laravel\Requests\Api;

use Session,Auth;
use App\Laravel\Requests\ApiRequestManager;

class SummernoteRequest extends ApiRequestManager{

	public function rules(){

		$id = $this->segment(3)?:0;

		$rules = [
			'file' => "image",
		];

		return $rules;
	}

	public function messages(){
		return [
			'required' => "This item is required.",
		];
	}
}