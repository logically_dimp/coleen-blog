<?php

namespace App\Laravel\Transformers;

use App\Laravel\Models\User;

use League\Fractal\TransformerAbstract;
use DB,Helper,Str,Cache,Carbon,Input;

use Illuminate\Support\Collection;
use App\Laravel\Transformers\MasterTransformer;

class MasterTransformer extends TransformerAbstract{
	
	protected $availableIncludes = [
		'user'
    ];

	public function transform($item){
		$result = [];
		foreach($item as $key => $value){
			$result[$key] = $value;
		}	

		return $result;
	}

	public function includeUser($item){
		$user_id = Input::get('user_id',0);
		$user = User::find($user_id) ? : new User;
        if(is_null($user->id)){ $user->id = 0;}
        return $this->item($user, new UserTransformer);
    }
}