<?php namespace App\Laravel\Listeners;

use App\Laravel\Events\LogUserAction;

class LogUserActionListener{

	public function handle(LogUserAction $log){
		$log->job();
	}
}