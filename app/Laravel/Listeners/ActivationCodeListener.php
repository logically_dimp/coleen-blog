<?php namespace App\Laravel\Listeners;

use App\Laravel\Events\EmailActivationCode;

class ActivationCodeListener{

	public function handle(EmailActivationCode $email){
		$email->job();
	}
}