<?php 

namespace App\Laravel\Middleware\Backoffice;

use Closure, Route, Session;
use Illuminate\Contracts\Auth\Guard;

class Authorize {

	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;

	/**
	 * Create a new filter instance.
	 *
	 * @param  Guard  $auth
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		$user = $this->auth->user();
		$route_name = Route::currentRouteName();

		$access_levels = [
			'backoffice.dashboard' => ['super_user', 'admin', 'teller', 'tv', 'receptionist'],
			'backoffice.admins.index' => ['super_user', 'admin'],
			'backoffice.admins.create' => ['super_user', 'admin'],
			'backoffice.admins.edit' => ['super_user', 'admin'],
			'backoffice.app_settings.index' => ['super_user', 'admin'],
			'backoffice.app_settings.create' => ['super_user', 'admin'],
			'backoffice.app_settings.edit' => ['super_user', 'admin'],
			'backoffice.articles.index' => ['super_user', 'admin'],
			'backoffice.articles.create' => ['super_user', 'admin'],
			'backoffice.articles.edit' => ['super_user', 'admin'],
			'backoffice.events.index' => ['super_user', 'admin'],
			'backoffice.events.create' => ['super_user', 'admin'],
			'backoffice.events.edit' => ['super_user', 'admin'],
			'backoffice.users.index' => ['super_user', 'admin', 'receptionist'],
			'backoffice.users.create' => ['super_user', 'admin', 'receptionist'],
			'backoffice.users.edit' => ['super_user', 'admin', 'receptionist'],
			'backoffice.citizen_reports.index' => ['super_user', 'admin'],
			'backoffice.citizen_reports.edit' => ['super_user', 'admin'],
			'backoffice.directories.index' => ['super_user', 'admin'],
			'backoffice.directories.create' => ['super_user', 'admin'],
			'backoffice.directories.edit' => ['super_user', 'admin'],
			'backoffice.establishments.index' => ['super_user', 'admin'],
			'backoffice.establishments.create' => ['super_user', 'admin'],
			'backoffice.establishments.edit' => ['super_user', 'admin'],
			'backoffice.emergencies.index' => ['super_user', 'admin'],
			'backoffice.emergencies.create' => ['super_user', 'admin'],
			'backoffice.emergencies.edit' => ['super_user', 'admin'],
			'backoffice.services.index' => ['super_user', 'admin'],
			'backoffice.services.create' => ['super_user', 'admin'],
			'backoffice.services.edit' => ['super_user', 'admin'],
			'backoffice.sub-services.index' => ['super_user', 'admin'],
			'backoffice.sub-services.create' => ['super_user', 'admin'],
			'backoffice.sub-services.edit' => ['super_user', 'admin'],
			'backoffice.pages.index' => ['super_user', 'admin'],
			'backoffice.pages.create' => ['super_user', 'admin'],
			'backoffice.pages.edit' => ['super_user', 'admin'],
			'backoffice.moments.index' => ['super_user', 'admin'],
			'backoffice.moments.create' => ['super_user', 'admin'],
			'backoffice.moments.edit' => ['super_user', 'admin'],
			'backoffice.widgets.index' => ['super_user', 'admin'],
			'backoffice.widgets.create' => ['super_user', 'admin'],
			'backoffice.widgets.edit' => ['super_user', 'admin'],
			'backoffice.schools.index' => ['super_user', 'admin'],
			'backoffice.schools.create' => ['super_user', 'admin'],
			'backoffice.schools.edit' => ['super_user', 'admin'],
			'backoffice.report_footers.index' => ['super_user', 'admin'],
			'backoffice.report_footers.create' => ['super_user', 'admin'],
			'backoffice.report_footers.edit' => ['super_user', 'admin'],
			'backoffice.queue.index' => ['super_user', 'admin','receptionist'],
			'backoffice.queue.tv' => ['super_user', 'admin','tv'],
			'backoffice.queue.queue' => ['super_user', 'teller'],
			'backoffice.surveys.index' => ['super_user', 'admin'],
			'backoffice.surveys.show' => ['super_user', 'admin'],
			'backoffice.reports.index' => ['super_user', 'admin'],
			'backoffice.reports.generate' => ['super_user', 'admin'],
			'backoffice.citizen_requests.index' => ['super_user', 'admin', 'teller'],
			'backoffice.citizen_requests.create' => ['super_user', 'admin', 'teller'],
			'backoffice.citizen_requests.edit' => ['super_user', 'admin', 'teller'],
			'backoffice.citizen_requests.transaction' => ['super_user', 'admin', 'teller'],
			'backoffice.citizen_requests.tracker' => ['super_user', 'admin', 'teller'],
		];
		
		if(array_key_exists($route_name, $access_levels) AND !in_array($user->type, $access_levels[$route_name])){
			Session::flash('notification-status','failed');
			Session::flash('notification-title',"Access Denied");
			Session::flash('notification-msg','You are not allowed to view the page you are tring to access.');
			return redirect()->route('backoffice.dashboard');
		}

		return $next($request);
	}

}
