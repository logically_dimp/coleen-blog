<?php 

namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Blog;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\BlogRequest;

/**
*
* Additional classes needed by this controller
*/
use Helper, ImageUploader, Carbon, Session, Str, DB;

class BlogController extends Controller{


	/**
	*
	* @var array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['statuses'] = [ '' => "Choose status", 'draft' => "Draft", 'published' => "Published"];
	}

	public function index () {
		$paginate = 5;
		$this->data['blogs'] = Blog::where('status',"published")
			->orderBy('updated_at',"DESC")->paginate($paginate);
		return view('backoffice.blogs.index',$this->data);
	}

	public function all () {
		$this->data['blogs'] = Blog::where('status',"published")
			->orderBy('updated_at',"DESC")->get();
		return view('backoffice.blogs.index',$this->data);
	}

	public function main () {
		$this->data['blogs'] = Blog::orderBy('updated_at',"DESC")->get();
		return view('backoffice.posts.index',$this->data);
	}

	public function create () {
		return view('backoffice.posts.create',$this->data);
	}

	public function store (BlogRequest $request) {
		try {
			$new_blog = new Blog;
			$new_blog->fill($request->all());
			$new_blog->user_id = $this->data["auth"]->id;
			$new_blog->posted_at = Helper::datetime_db($request->get('posted_at'));
			$new_blog->slug = Helper::get_slug('blog','title',$request->get('title'));
			$new_blog->excerpt = Helper::get_excerpt($request->get('content'));
			if($request->hasFile('file')) $new_blog->fill(ImageUploader::upload($request, 'uploads/blog',"file"));

			if($new_blog->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A blog has been added.");
				return redirect()->route('backoffice.posts.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$blog = Blog::find($id);

		if (!$blog) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.posts.index');
		}

		$this->data['blog'] = $blog;
		return view('backoffice.posts.edit',$this->data);
	}

	public function posts ($id = NULL) {
		$blog = Blog::find($id);

		if (!$blog) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.posts.index');
		}

		$this->data['blogs'] = Blog::where('id',$id)->first();
		return view('backoffice.blogs.post',$this->data);
	}

	public function update (BlogRequest $request, $id = NULL) {
		try {
			$blog = Blog::find($id);

			if (!$blog) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.posts.index');
			}

			$old_title = $blog->title;

			$blog->fill($request->all());
			$blog->posted_at = Helper::datetime_db($request->get('posted_at'));
			$blog->excerpt = Helper::get_excerpt($request->get('content'));
			
			if($old_title != $request->get('title')){
				$blog->slug = Helper::get_slug('article','title',$request->get('title'));
			}
			
			if($request->hasFile('file')) $blog->fill(ImageUploader::upload($request, 'uploads/blog',"file"));

			if($blog->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A blog has been updated.");
				return redirect()->route('backoffice.posts.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$blog = Blog::find($id);

			if (!$blog) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.posts.index');
			}

			if($blog->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A blog has been deleted.");
				return redirect()->route('backoffice.posts.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}
}