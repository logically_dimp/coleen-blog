<?php 

namespace App\Laravel\Controllers\Backoffice;

/*
*
* Models used for this controller
*/


/*
*
* Requests used for validating inputs
*/


/*
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str, DB, Curl, Input;

class HomeController extends Controller{

	/*
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
	}

	public function index () {
		return view('backoffice.home',$this->data);
	}

	public function google () {
		return view('backoffice.google673373b13476671e');
	}

}