<?php 

namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\User;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\UserRequest;

/**
*
* Additional classes needed by this controller
*/
use Helper, ImageUploader, Carbon, Session, Str, DB, Event;
use App\Laravel\Events\EmailActivationCode;

class UserController extends Controller{


	/**
	*
	* @var array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['barangays'] = ['' => "Choose barangay"] + Helper::get_barangays();
		$this->data['genders'] = [ '' => "Choose gender", 'male' => "Male", 'female' => "Female"];
	}

	private function _generate_username($str){
		$new_username = Helper::str_clean(Str::lower($str));
		$check_username = User::where('username', 'like', "%{$str}%")->withTrashed()->count();
		if($check_username) $new_username = $new_username.($check_username + 1);
		return $new_username;
	}

	public function index () {
		$this->data['users'] = User::whereNotIn('type',["super_user"])->orderBy('updated_at',"DESC")->get();
		return view('backoffice.users.index',$this->data);
	}

	public function create () {
		return view('backoffice.users.create',$this->data);
	}

	public function store (UserRequest $request) {
		try {
			$new_user = new User;
			$new_user->fill($request->all());
			$new_user->username = $this->_generate_username($request->get('fname'));
			$new_user->email = Str::lower($request->get('email'));
			$new_user->password = bcrypt($request->get('password'));
			$new_user->type = "user";

			$birthdate = $request->get('birthdate');
			if($birthdate) {
				$new_user->age = Carbon::parse($birthdate)->age;
			}

			if($request->hasFile('file')) $new_user->fill(ImageUploader::upload($request, 'uploads/user',"file"));

			if($new_user->save()) {

				// $notification_data = new EmailActivationCode(['user_id' => $new_user->id]);
				// Event::fire('activation-code', $notification_data);

				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A user has been added.");
				return redirect()->route('backoffice.users.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$user = User::where('type',"user")->find($id);

		if (!$user) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.users.index');
		}

		// $tracked_requests = UserTracker::where('user_id', $user->id)->pluck('citizen_request_id');
		// $this->data['requests'] = CitizenRequest::whereIn('id', $tracked_requests)->orderBy('created_at',"DESC")->get();
		$this->data['user'] = $user;
		return view('backoffice.users.edit',$this->data);
	}

	public function update (UserRequest $request, $id = NULL) {
		try {
			$user = User::where('type',"user")->find($id);

			if (!$user) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.users.index');
			}

			$user->fill($request->all());
			$user->email = Str::lower($request->get('email'));

			if($request->get('password', FALSE)) {
				$user->password = bcrypt($request->get('password'));
			}

			$birthdate = $request->get('birthdate');
			if($birthdate) {
				$user->age = Carbon::parse($birthdate)->age;
			}

			if($request->hasFile('file')) $user->fill(ImageUploader::upload($request, 'uploads/user',"file"));

			if($user->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A user has been updated.");
				return redirect()->route('backoffice.users.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$user = User::where('type',"user")->find($id);

			if (!$user) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.users.index');
			}

			if($user->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A user has been deleted.");
				return redirect()->route('backoffice.users.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}
}