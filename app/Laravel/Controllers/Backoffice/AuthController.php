<?php 

namespace App\Laravel\Controllers\Backoffice;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;

use App\Laravel\Models\User;

use Session, Input, Auth;

class AuthController extends Controller{

	protected $data;

	public function __construct(Guard $auth){
		$this->auth = $auth;
		$this->data['auth'] = $auth;
	}

	public function login(){
		return view('backoffice.auth.login',$this->data);
	}

	public function lock() {
		$user = $this->auth->user();
		$user->is_lock = 1;
		$user->save();
		$this->data['auth'] = $user;
		return view('backoffice.auth.lock',$this->data);
	}

	public function unlock() {
		try {
			$user = $this->auth->user();
			$password = Input::get('password');
			$remember_me = Input::get('remember_me',0);

			if($this->auth->attempt(['username' => $user->username,'password' => $password], $remember_me)){
				$user->is_lock = 0;
				$user->save();
				Session::flash('notification-status','info');
				Session::flash('notification-title',"It's nice to be back");
				Session::flash('notification-msg',"Welcome {$this->auth->user()->name}!");

				switch ($user->type) {
					case 'tv': return redirect()->route('backoffice.queue.tv'); break;
					case 'receptionist': return redirect()->route('backoffice.queue.index'); break;
					case 'teller': return redirect()->route('backoffice.queue.queue'); break;
					default: return redirect()->intended('/'); break;
				}
				
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Invalid password.');
			return redirect()->back();

		} catch (Exception $e) {
			abort(500);
		}
	}

	public function activate($activation_code = NULL) {

		$user = User::where('activation_code', $activation_code)->where('activation_code', "<>", "")->first();

		if($user){
			$user->is_active = "yes";
			$user->save();
			$this->data['message'] = "Success! Your account is now activated, you may now close this window.";
		} else {
			$this->data['message'] = "Error. This activation code is invalid, close this window and try the link provided in your email.";
		}
		return view('backoffice.activation', $this->data);
	}

	public function authenticate(){
		try{
			$username = Input::get('username');
			$password = Input::get('password');
			$remember_me = Input::get('remember_me',0);
			$field = filter_var($username, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';	

			if($this->auth->attempt([$field => $username,'password' => $password], $remember_me)){

				if(!in_array($this->auth->user()->type, ["super_user","admin","receptionist","tv","teller"])){
					$this->auth->logout();
					Session::flash('notification-status','failed');
					Session::flash('notification-title',"Unauthorized access!");
					Session::flash('notification-msg',"You don't have enough authorization access.");
					return redirect()->route('backoffice.login');
				}

				$user = $this->auth->user();
				$user->is_lock = 0;
				$user->save();

				Session::flash('notification-status','info');
				Session::flash('notification-title',"It's nice to be back");
				Session::flash('notification-msg',"Welcome {$this->auth->user()->name}!");

				switch ($user->type) {
					case 'tv': return redirect()->route('backoffice.queue.tv'); break;
					case 'receptionist': return redirect()->route('backoffice.queue.index'); break;
					case 'teller': return redirect()->route('backoffice.queue.queue'); break;
					default: return redirect()->intended('/'); break;
				}
			}	

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Wrong username or password.');
			return redirect()->back();

		}catch(Exception $e){
			abort(500);
		}
	}

	public function destroy(){
		$this->auth->logout();
		Session::flash('notification-status','success');
		Session::flash('notification-msg','You are now signed off.');
		return redirect()->route('backoffice.login');
	}
}