<?php 

namespace App\Laravel\Controllers\Backoffice;

use Illuminate\Support\Collection;
use App\Laravel\Controllers\Controller as MainController;
use Auth, Session;

class Controller extends MainController{

	protected $data;

	public function __construct(){
		self::set_backoffice_routes();
		self::set_user_info();
	}

	public function set_backoffice_routes(){
		$this->data['routes'] = array(
			"blogs" => ['backoffice.blogs.index','backoffice.blogs.post'],
			"dashboard" => ['backoffice.dashboard'],
			"users" => ['backoffice.users.index','backoffice.users.create','backoffice.users.edit'],
			"admins" => ['backoffice.admins.index','backoffice.admins.create','backoffice.admins.edit'],
			"posts" => ['backoffice.posts.index','backoffice.posts.create','backoffice.posts.edit'],
		);
	}

	public function set_user_info(){
		$this->data['auth'] = Auth::user();
	}
	
	public function get_data(){
		return $this->data;
	}
}