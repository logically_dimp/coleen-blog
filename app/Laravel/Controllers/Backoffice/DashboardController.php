<?php 

namespace App\Laravel\Controllers\Backoffice;

/*
*
* Models used for this controller
*/
use App\Laravel\Models\User;


/*
*
* Requests used for validating inputs
*/


/*
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str, DB, Curl, Input;

class DashboardController extends Controller{

	/*
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
	}

	public function index () {

		// $this->data['all_user'] = User::all(); 
		// $this->data['today'] = Helper::date_db(Carbon::now());
		// $this->data['all_request'] = CitizenRequest::all();

		// $this->data['month_list'] = [];
		// $this->data['completed_list'] = [];
		// $this->data['all_list'] = [];
		// $this->data['medical'] = [];
		// $this->data['burial'] = [];
		// $this->data['scholarship'] = [];
		// $this->data['total_request'] = 0 ;
		// $this->data['average_request'] = 0;
		// $this->data['quickest_request'] = 0;
		// $this->data['quickest_duration'] = 0;
		// $sum = 0;

		// foreach(range(0,5) as $index => $value){
		// 	$date_today = Carbon::now();
		// 	$from = Helper::date_db($date_today->subMonths($value)->startOfMonth());
		// 	$to = Helper::date_db($date_today->endOfMonth());
		// 	$completed = CitizenRequest::whereRaw("DATE(updated_at) BETWEEN '{$from}' AND '{$to}'")
		// 							->where('status',"done")->get();
		// 	$all = CitizenRequest::whereRaw("DATE(updated_at) BETWEEN '{$from}' AND '{$to}'")
		// 							->get();

		// 	array_push($this->data['month_list'] ,$from);
		// 	array_push($this->data['completed_list'] ,$completed->count());
		// 	array_push($this->data['all_list'] ,$all->count());

		// 	array_push($this->data['medical'], $all->where('subcategory',"medical")->count());
		// 	array_push($this->data['burial'], $all->where('subcategory',"burial")->count());
		// 	array_push($this->data['scholarship'], $all->where('subcategory',"scholarship")->count());

		// 	if($index == 0) $this->data['this_month'] = $all->count();

		// 	foreach($completed as $index => $item){
		// 		if($index == 0 ){
		// 			$this->data['quickest_request'] = $item;
		// 			$this->data['quickest_duration'] = $item->tracker->total_duration;
		// 		}else{
		// 			if($this->data['quickest_duration'] > $item->tracker->total_duration){
		// 				$this->data['quickest_request'] = $item;
		// 				$this->data['quickest_duration'] = $item->tracker->total_duration;
		// 			}
		// 		}
		// 		$sum += $item->tracker->total_duration;
		// 	}


		// 	$this->data['total_request'] += $completed->count();
		// }

		// if($sum > 0 AND $this->data['total_request'] > 0){
		// 	$this->data['average_request'] =  round($sum / $this->data['total_request']);
		// }

		return view('backoffice.dashboard',$this->data);
	}

	public function google () {
		return view('backoffice.google673373b13476671e');
	}

}