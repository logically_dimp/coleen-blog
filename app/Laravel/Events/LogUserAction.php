<?php namespace App\Laravel\Events;

use Illuminate\Queue\SerializesModels;
use App\Laravel\Models\UserLog;
use App\Laravel\Models\User;

class LogUserAction extends Event {

	use SerializesModels;

	/**
	 * Create a new event instance.
	 *
	 * @return void
	 */
	public function __construct(array $form_data)
	{
		$this->user_id = $form_data['user_id'];
		$this->data = $form_data['data'];
	}

	public function job(){
		$user = User::find($this->user_id);
		if($user){
			$log = new UserLog;
			$log->user_id = $this->user_id;
			$log->fill($this->data);
			$log->save();
		}
	}

}
