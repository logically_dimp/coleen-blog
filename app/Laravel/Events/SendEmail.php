<?php namespace App\Laravel\Events;

use Illuminate\Queue\SerializesModels;
use Mail,Str;
// use App\Constech\Models\GeneralSetting;

class SendEmail extends Event {

	use SerializesModels;

	/**
	 * Create a new event instance.
	 *
	 * @return void
	 */
	public function __construct(array $form_data)
	{
		$this->name = Str::title($form_data['name']);
		$this->subject = Str::title($form_data['subject']);
		// $this->company = Str::title($form_data['company']);
		$this->email = $form_data['email'];
		$this->message = $form_data['message'];
		$this->contact_number = $form_data['contact_number'];
		// $this->setting = GeneralSetting::where('code','email-inquiry')->first();
		$this->setting = (object)["content" => "support@highlysucceed.com"];
	}

	public function job(){
		// $form_data = $this->data;
		$data = ['name' => $this->name,'subject' => $this->subject,
				 'email' => $this->email,'msg' => $this->message,
				 'contact_number' => $this->contact_number
				];


		Mail::send('emails.support', $data, function($message){
			$message->from($this->email,"{$this->name}");
			$message->to($this->setting->content,"Highly Succeed Support Team");
		   	if($this->subject){$message->subject($this->subject); }
		   	else{  $message->subject("Contact Inquiry"); }
		   
		});
	}

}
