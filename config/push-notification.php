<?php

return array(

    'appNameIOS'     => array(
        'environment' =>'development',
        'certificate' =>base_path('resources/certificate/kwtg.pem'),
        'passPhrase'  =>'knowhere123',
        'service'     =>'apns'
    ),
    'appNameAndroid' => array(
        'environment' =>'production',
        'apiKey'      =>'AIzaSyCczDm2EW54qzg0uUmqY-AuExuhMBodzfw',
        'service'     =>'gcm'
    )

);